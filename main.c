/*
 Description 	    Notation 	Time 	            Width/Freq
Pixel Clock 	    tclk 	    39.7 ns (± 0.5%) 	25.175MHz
Hor Sync Time 	    ths 	    3.813 μs 	        96 Pixels
Hor Back Porch 	    thbp 	    1.907 μs 	        48 Pixels
Hor Front Porch 	thfp 	    0.636 μs 	        16 Pixels
Hor Addr Video Time thaddr 	    25.422 μs 	        640 Pixels
Hor L/R Border 	    thbd 	    0 μs 	            0 Pixels
----------------------------------------------      31.778 us

V Sync Time 	    tvs 	    0.064 ms 	        2 Lines
V Back Porch 	    tvbp 	    1.048 ms 	        33 Lines
V Front Porch 	    tvfp 	    0.318 ms 	        10 Lines
V Addr Video Time 	tvaddr 	    15.253 ms 	        480 Lines
V T/B Border 	    tvbd 	    0 ms 	            0 Lines
----------------------------------------------      16.683 ms
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/dma.h"

#include "h_sync.pio.h"
#include "v_sync.pio.h"
#include "color_rgb.pio.h"

#define HOR_ACTIVE 655        // (active + frontporch - 1) - one cycle delamovy for
#define VER_ACTIVE 479        // (active - 1)
#define RGB_ACTIVE 319        // (horizontal active)/2 - 1e
#define SIZE_VGA_ARRAY 153600 // Total pixels/2

unsigned char vga_data_disp_array[SIZE_VGA_ARRAY];
char *vga_array_pointer = vga_data_disp_array;

enum pins
{
    HSYNC_PIN = 11,
    VSYNC_PIN = 10,
    RED_PIN = 12,
    GREEN_PIN = 13,
    BLUE_PIN = 14
};

enum colors
{
    BLACK = 0,
    RED = 1,
    GREEN = 2,
    YELLOW = 3,
    BLUE = 4,
    MAGENTA = 5,
    CYAN = 6,
    WHITE = 7
};

void draw_two_pixels(uint32_t a_index_array, char a_two_pixels_color)
{
    vga_data_disp_array[a_index_array] |= a_two_pixels_color;
    vga_data_disp_array[a_index_array] |= a_two_pixels_color << 3;
}

void draw_one_pixel(uint32_t a_x_cord, uint32_t a_y_cord, char a_color_pixel)
{
    uint32_t curr_index_array = a_x_cord + ((RGB_ACTIVE * 2 + 2) * a_y_cord);

    if (curr_index_array % 2)
    {
        vga_data_disp_array[curr_index_array >> 1] |= (a_color_pixel << 3);
    }
    else
    {
        vga_data_disp_array[curr_index_array >> 1] |= a_color_pixel;
    }
}

int main()
{
    const uint LED_PIN = PICO_DEFAULT_LED_PIN;

    PIO pio_instance = pio0;

    uint hsync_offset = pio_add_program(pio_instance, &h_sync_program);
    uint vsync_offset = pio_add_program(pio_instance, &v_sync_program);
    uint rgb_offset = pio_add_program(pio_instance, &color_rgb_program);

    uint hsync_sm = 0;
    uint vsync_sm = 1;
    uint rgb_sm = 2;

    int rgb_dma_channel_0 = 0;
    int rgb_dma_channel_1 = 1;

    dma_channel_config dma_channel_0_config = dma_channel_get_default_config(rgb_dma_channel_0);
    dma_channel_config dma_channel_1_config = dma_channel_get_default_config(rgb_dma_channel_1);

    stdio_init_all();
    setup_default_uart();

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    for (int i = 0; i < 640; i++)
    {
        for (int j = 0; j < 480; j++)
        {
            if (i < 320 && j < 240)
            {
                draw_one_pixel(i, j, RED);
            }
            else if (i >= 320 && j < 240)
            {
                draw_one_pixel(i, j, BLUE);
            }
            else if (i < 320 && j >= 240)
            {
                draw_one_pixel(i, j, YELLOW);
            }
            else if (i >= 320 && j >= 240)
            {
                draw_one_pixel(i, j, GREEN);
            }
        }
    }

    h_sync_program_init(pio_instance, hsync_sm, hsync_offset, HSYNC_PIN);
    v_sync_program_init(pio_instance, vsync_sm, vsync_offset, VSYNC_PIN);
    color_rgb_program_init(pio_instance, rgb_sm, rgb_offset, RED_PIN);

    channel_config_set_read_increment(&dma_channel_0_config, true);
    channel_config_set_read_increment(&dma_channel_1_config, false);

    channel_config_set_chain_to(&dma_channel_0_config, rgb_dma_channel_1);
    channel_config_set_chain_to(&dma_channel_1_config, rgb_dma_channel_0);

    channel_config_set_transfer_data_size(&dma_channel_0_config, DMA_SIZE_8);
    channel_config_set_transfer_data_size(&dma_channel_1_config, DMA_SIZE_32);

    channel_config_set_write_increment(&dma_channel_0_config, false);
    channel_config_set_write_increment(&dma_channel_1_config, false);

    channel_config_set_dreq(&dma_channel_0_config, DREQ_PIO0_TX2);

    dma_channel_configure(rgb_dma_channel_0, &dma_channel_0_config, &pio_instance->txf[rgb_sm], &vga_data_disp_array, SIZE_VGA_ARRAY, false);
    dma_channel_configure(rgb_dma_channel_1, &dma_channel_1_config, &dma_hw->ch[rgb_dma_channel_0].read_addr, &vga_array_pointer, 1, false);

    pio_sm_put_blocking(pio_instance, hsync_sm, HOR_ACTIVE);
    pio_sm_put_blocking(pio_instance, vsync_sm, VER_ACTIVE);
    pio_sm_put_blocking(pio_instance, rgb_sm, RGB_ACTIVE);

    pio_enable_sm_mask_in_sync(pio_instance, ((1u << hsync_sm) | (1u << vsync_sm) | ((1u << rgb_sm))));
    dma_start_channel_mask((1u << rgb_dma_channel_0));

    while (1)
    {
        printf("Raspberry Pi Pico - works. Curr time: %.2f [s]\n", time_us_32() / 1000000);
        gpio_put(LED_PIN, 1);
        sleep_ms(500);
        gpio_put(LED_PIN, 0);
        sleep_ms(500);
    }
    return 0;
}