/*
 Description 	    Notation 	Time 	            Width/Freq
Pixel Clock 	    tclk 	    39.7 ns (± 0.5%) 	25.175MHz
Hor Sync Time 	    ths 	    3.813 μs 	        96 Pixels
Hor Back Porch 	    thbp 	    1.907 μs 	        48 Pixels
Hor Front Porch 	thfp 	    0.636 μs 	        16 Pixels
Hor Addr Video Time thaddr 	    25.422 μs 	        640 Pixels
Hor L/R Border 	    thbd 	    0 μs 	            0 Pixels
----------------------------------------------      31.778 us

V Sync Time 	    tvs 	    0.064 ms 	        2 Lines
V Back Porch 	    tvbp 	    1.048 ms 	        33 Lines
V Front Porch 	    tvfp 	    0.318 ms 	        10 Lines
V Addr Video Time 	tvaddr 	    15.253 ms 	        480 Lines
V T/B Border 	    tvbd 	    0 ms 	            0 Lines
----------------------------------------------      16.683 ms
*/

.program v_sync
.side_set 1 opt

pull block                      ; Copy number of the active vertical lines 
.wrap_target                    ; Program wraps to here

set pins 0                      ; Sync pulse - set pin to 0 
irq wait 0                      ; wait twice for irq from h_sync line - 64 us 
irq wait 0

; set pins 1 
set x 31 side 1                 ; init x to 31 and set pin to 1 
backporch:                      ; Back porch
    irq wait 0                  ; wait for irq from h_sync line
    jmp x-- backporch 
  
mov y, osr                      ; Copy number of the active vertical lines from osr to y 
active_pixels:                  ; Front porch
    irq wait 0
    irq 1                       ; Signal for RGB drawing 
    jmp y-- active_pixels

set x 9                         ; init x to 9                            
front_porch:                    ; Front porch
    irq wait 0  
    jmp x-- front_porch

.wrap                             

// C code to initialize the PIO state machine

% c-sdk {
static inline void v_sync_program_init(PIO pio, uint sm, uint offset, uint pin) 
{
    pio_sm_config c = v_sync_program_get_default_config(offset);
    sm_config_set_set_pins(&c, pin, 1);
    sm_config_set_sideset_pins(&c, pin);
    sm_config_set_clkdiv(&c, 5) ;
    pio_gpio_init(pio, pin);
    pio_sm_set_consecutive_pindirs(pio, sm, pin, 1, true);
    pio_sm_init(pio, sm, offset, &c);
}
%}
