# Set minimum required version of CMake
cmake_minimum_required(VERSION 3.12)

# Include build functions from Pico SDK
include($ENV{PICO_SDK_PATH}/external/pico_sdk_import.cmake)
# include(pico-sdk/pico_sdk_init.cmake)

# Set name of project (as PROJECT_NAME) and C/C   standards
project(vga_interface_pio C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

# Creates a pico-sdk subdirectory in our project for the libraries
pico_sdk_init()

#######

# Tell CMake where to find the executable source file
add_executable(${PROJECT_NAME} 
    main.c
)

# must match with pio filename and executable name from above
pico_generate_pio_header(${PROJECT_NAME}  ${CMAKE_CURRENT_LIST_DIR}/h_sync.pio)
pico_generate_pio_header(${PROJECT_NAME}  ${CMAKE_CURRENT_LIST_DIR}/v_sync.pio)
pico_generate_pio_header(${PROJECT_NAME}  ${CMAKE_CURRENT_LIST_DIR}/color_rgb.pio)

# Create map/bin/hex/uf2 files
pico_add_extra_outputs(${PROJECT_NAME})

# must match with executable name and source file names
target_sources(${PROJECT_NAME} PRIVATE 
    main.c
)

# must match with executable name
target_link_libraries(${PROJECT_NAME} PRIVATE pico_stdlib hardware_pio hardware_dma)

# must match with executable name
pico_add_extra_outputs(
    ${PROJECT_NAME}
)
