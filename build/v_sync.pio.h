// -------------------------------------------------- //
// This file is autogenerated by pioasm; do not edit! //
// -------------------------------------------------- //

#pragma once

#if !PICO_NO_HARDWARE
#include "hardware/pio.h"
#endif

// ------ //
// v_sync //
// ------ //

#define v_sync_wrap_target 1
#define v_sync_wrap 13

static const uint16_t v_sync_program_instructions[] = {
    0x80a0, //  0: pull   block                      
            //     .wrap_target
    0xe000, //  1: set    pins, 0                    
    0xc020, //  2: irq    wait 0                     
    0xc020, //  3: irq    wait 0                     
    0xf83f, //  4: set    x, 31           side 1     
    0xc020, //  5: irq    wait 0                     
    0x0045, //  6: jmp    x--, 5                     
    0xa047, //  7: mov    y, osr                     
    0xc020, //  8: irq    wait 0                     
    0xc001, //  9: irq    nowait 1                   
    0x0088, // 10: jmp    y--, 8                     
    0xe029, // 11: set    x, 9                       
    0xc020, // 12: irq    wait 0                     
    0x004c, // 13: jmp    x--, 12                    
            //     .wrap
};

#if !PICO_NO_HARDWARE
static const struct pio_program v_sync_program = {
    .instructions = v_sync_program_instructions,
    .length = 14,
    .origin = -1,
};

static inline pio_sm_config v_sync_program_get_default_config(uint offset) {
    pio_sm_config c = pio_get_default_sm_config();
    sm_config_set_wrap(&c, offset + v_sync_wrap_target, offset + v_sync_wrap);
    sm_config_set_sideset(&c, 2, true, false);
    return c;
}

static inline void v_sync_program_init(PIO pio, uint sm, uint offset, uint pin) 
{
    pio_sm_config c = v_sync_program_get_default_config(offset);
    sm_config_set_set_pins(&c, pin, 1);
    sm_config_set_sideset_pins(&c, pin);
    sm_config_set_clkdiv(&c, 5) ;
    pio_gpio_init(pio, pin);
    pio_sm_set_consecutive_pindirs(pio, sm, pin, 1, true);
    pio_sm_init(pio, sm, offset, &c);
}

#endif

